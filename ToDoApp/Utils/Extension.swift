//
//  Extension.swift
//  ToDoApp
//
//  Created by Vaibhav on 28/06/21.
//

import Foundation
import UIKit

extension UIView {
   
    public func addViewShadow() {
        self.layer.shadowColor = UIColor(red: 156/255, green: 179/255, blue: 210/255, alpha: 1.0) .cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
    }
}
