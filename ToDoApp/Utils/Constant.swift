//
//  Constant.swift
//  ToDoApp
//
//  Created by Vaibhav on 28/06/21.
//

import Foundation

struct Constant {
    
    static let TODAY = "Today"
    static let TOMORROW = "Tomorrow"
    static let UPCOMING = "Upcoming"
    static let Task_Table_Cell = "taskTableViewCell"
    
}
