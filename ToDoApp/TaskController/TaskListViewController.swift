//
//  TaskListViewController.swift
//  ToDoApp
//
//  Created by Vaibhav on 27/06/21.
//

import UIKit
import  CoreData

class TaskListViewController: UIViewController {
    
    @IBOutlet var uiTaskSegment: UISegmentedControl!
    @IBOutlet var uiTaskListTableVw: UITableView!
    
    var todayTaskArr = [[String : Any]]()
    var tomorrowTaskArr = [[String : Any]]()
    var upComingTaskArr = [[String : Any]]()
    var eventsArr = [[String : Any]]()
    var selectedSegmentStr : String = ""
    var idNo : Int64 = 0
    
    //Reference to managed object context
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //Data for the table
    var items:[Task]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiTaskSegment.selectedSegmentIndex = 0
        selectedSegmentStr = Constant.TODAY
        
        uiTaskListTableVw.tableFooterView = UIView()
        
        uiTaskSegment.addTarget(self, action: #selector(taskDayClick), for: .valueChanged)
        
        //Get items from core data
        fetchTask()
        
        self.title = "Task List"
    }
    
    func fetchTask() {
        
        todayTaskArr.removeAll()
        tomorrowTaskArr.removeAll()
        upComingTaskArr.removeAll()
        eventsArr.removeAll()
        
        //Fetch data from core data to display data in tableview
        do
        {
            self.items = try context.fetch(Task.fetchRequest())
            
            if self.items != nil
            {
                idNo = Int64(self.items!.count)
                
                for index in 0..<self.items!.count
                {
                    let task = self.items![index]
                    
                    var taskDict = [String : Any]()
                    taskDict["title"] = task.title
                    taskDict["descript"] = task.descript
                    taskDict["day"] = task.day
                    taskDict["id"] = task.id
                    
                    if task.day == Constant.TODAY
                    {
                        todayTaskArr.append(taskDict)
                    }
                    else if task.day == Constant.TOMORROW
                    {
                        tomorrowTaskArr.append(taskDict)
                    }
                    else
                    {
                        upComingTaskArr.append(taskDict)
                    }
                }
                
                if selectedSegmentStr == Constant.TODAY
                {
                    eventsArr = todayTaskArr
                }
                else if selectedSegmentStr == Constant.TOMORROW
                {
                    eventsArr = tomorrowTaskArr
                }
                else
                {
                    eventsArr = upComingTaskArr
                }
            }
            
            
            DispatchQueue.main.async {
                self.uiTaskListTableVw.reloadData()
            }
        }
        catch
        {
            
        }
    }
    
    @IBAction func onAddBarButtonClick(_ sender: Any) {
        
        alertWithTF()
    }
    
    func alertWithTF() {
        
        //Step : 1
        let alert = UIAlertController(title: "Add Task", message: "", preferredStyle: UIAlertController.Style.alert )
        //Step : 2
        let save = UIAlertAction(title: "Save", style: .default) { (alertAction) in
            let titleTxtField = alert.textFields![0] as UITextField
            let descTxtField = alert.textFields![1] as UITextField
            
            let newTask = Task(context: self.context)
            
            if titleTxtField.text != "" {
                //Read TextFields text data
                newTask.title = titleTxtField.text
                newTask.descript = descTxtField.text
                newTask.day = self.selectedSegmentStr
                newTask.id = self.idNo
                
                
                //Save the data
                do
                {
                    try self.context.save()
                }
                catch
                {
                    
                }
                
                //Refetch the data
                self.fetchTask()
                
            } else {
                //"TF 1 is Empty..."
            }
            
        }
        
        //Step : 3
        //For first TF
        alert.addTextField { (textField) in
            textField.placeholder = "Enter task title *"
            textField.textColor = .red
        }
        //For second TF
        alert.addTextField { (textField) in
            textField.placeholder = "Enter task description (optional)"
            textField.textColor = .blue
        }
        
        //Step : 4
        alert.addAction(save)
        
        //Cancel action
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in }
        alert.addAction(cancel)
        
        
        self.present(alert, animated:true, completion: nil)
        
    }
    
    @IBAction func taskDayClick(segControl: UISegmentedControl) {
        
        if segControl.selectedSegmentIndex == 0
        {
            selectedSegmentStr = Constant.TODAY
            eventsArr = todayTaskArr
        }
        else if segControl.selectedSegmentIndex == 1
        {
            selectedSegmentStr = Constant.TOMORROW
            eventsArr = tomorrowTaskArr
        }
        else
        {
            selectedSegmentStr = Constant.UPCOMING
            eventsArr = upComingTaskArr
        }
        
        
        DispatchQueue.main.async {
            self.uiTaskListTableVw.reloadData()
        }
        
    }
    
}

extension TaskListViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if eventsArr.count > 0
        {
            let taskDict = eventsArr[indexPath.row]
            let idNumber = taskDict["id"] as! Int64
            
            let alert = UIAlertController(title: "Edit Task", message: "Title is mandatory & description is optional.", preferredStyle: .alert)
            //For first TF
            alert.addTextField { (textField) in
                textField.textColor = .red
            }
            //For second TF
            alert.addTextField { (textField) in
                textField.textColor = .blue
            }
            
            
            //GET THE TEXTFIELD FOR THE ALERT
            let titleTxtField = alert.textFields![0] as UITextField
            titleTxtField.text = taskDict["title"] as? String ?? ""
            
            let descTxtField = alert.textFields![1] as UITextField
            descTxtField.text = taskDict["descript"] as? String ?? ""
            
            //Configure save button
            let saveBtn = UIAlertAction(title: "Save", style: .default) { (action) in
                
                //Get the textfield for the alert
                let titleTxtField = alert.textFields![0]
                let descTxtField = alert.textFields![1]
                
                
                if titleTxtField.text != "" {
                    
                    //Save the data
                    
                    for items in self.items!
                    {
                        if items.id == idNumber
                        {
                            items.title = titleTxtField.text
                            items.descript = descTxtField.text
                            
                            do
                                {
                                    try self.context.save()
                                }
                            catch
                            {
                                
                            }
                            
                            //Refetch the data
                            self.fetchTask()
                            
                            break
                        }
                    }
                }
            }
            
            //add button
            alert.addAction(saveBtn)
            
            //Cancel action
            let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in }
            alert.addAction(cancel)
            
            //show alert
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
}

extension TaskListViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return eventsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.Task_Table_Cell, for: indexPath) as! TaskTableViewCell
        
        if eventsArr.count > 0
        {
            if let tasks = eventsArr[indexPath.row] as? [String : Any]
            {
                if selectedSegmentStr == tasks["day"] as! String
                {
                    cell.uiTaskTitleLbl.text = tasks["title"] as? String ?? ""
                    cell.uiTaskDescLbl.text = tasks["descript"] as? String ?? ""
                }
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let taskDict = eventsArr[indexPath.row]
        let idNumber = taskDict["id"] as! Int64
        
        //Create swipe action
        let action = UIContextualAction(style: .destructive, title: "Delete") {(action, view, completionHandler) in
            
            if self.items != nil
            {
                for index in 0..<self.items!.count
                {
                    let items = self.items![index]
                    
                    if items.id == idNumber
                    {
                        //Task to remove
                        let taskToRemove = self.items![index]
                        
                        //Remove the task
                        self.context.delete(taskToRemove)
                        
                        //Save the data
                        do
                            {
                                try self.context.save()
                            }
                        catch
                        {
                            
                        }
                        
                        //Re-fetch the data
                        self.fetchTask()
                        
                        break
                    }
                    
                }
            }
        }
        
        
        //return swipe actions
        return UISwipeActionsConfiguration(actions: [action])
    }
}
