//
//  TaskTableViewCell.swift
//  ToDoApp
//
//  Created by Vaibhav on 27/06/21.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var uiTaskTitleLbl : UILabel!
    @IBOutlet weak var uiTaskDescLbl: UILabel!
    @IBOutlet var uiBaseVw: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uiBaseVw.layer.cornerRadius = 10
        uiBaseVw.addViewShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
