//
//  Task+CoreDataProperties.swift
//  ToDoApp
//
//  Created by Vaibhav on 27/06/21.
//
//

import Foundation
import CoreData


extension Task {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task")
    }

    @NSManaged public var title: String?
    @NSManaged public var descript: String?
    @NSManaged public var day: String?
    @NSManaged public var id: Int64

}

extension Task : Identifiable {

}
